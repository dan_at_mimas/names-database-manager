package uk.ac.mimas.names.databasemanager.types;

import uk.ac.mimas.names.disambiguator.types.NormalisedCollaboration;

public class Collaboration extends NormalisedCollaboration implements Summary{

	private int dbID;
	private int parentID;

	@Override
	public int getDbID() {
		return this.dbID;
	}

	@Override
	public void setDbID(int dbID) {
		this.dbID = dbID;
	}

	@Override
	public int getParentID() {
		return this.parentID;
	}

	@Override
	public void setParentID(int parentID) {
		this.parentID = parentID;
	}
	@Override
	public String serialise() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void deserialise(String serialised) {
		// TODO Auto-generated method stub
	}

}
