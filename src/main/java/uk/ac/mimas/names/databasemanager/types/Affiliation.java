package uk.ac.mimas.names.databasemanager.types;

import uk.ac.mimas.names.disambiguator.types.NormalisedAffiliation;
import uk.ac.mimas.names.disambiguator.types.NormalisedTitle;




public class Affiliation extends NormalisedAffiliation implements Summary {
	private int dbID;
	private int parentID;


	/**
	 * Constructor
	 */
	public Affiliation() {
		super();
	}

	/**
	 * Constructor
	 *
	 * @param dbID
	 * @param parentID
	 */
	public Affiliation(int dbID, int parentID) {
		super();
		this.dbID = dbID;
		this.parentID = parentID;
	}
	

	/**
     * Constructor
     * @param normalised the normalised affiliation to convert into a database affiliation
     **/
	public Affiliation(NormalisedAffiliation normalised){
		this.dbID = 0;
		this.parentID = 0;
		this.sourceID = normalised.getSourceID();
		this.sourceName = normalised.getSourceName();
		this.sourceURL = normalised.getSourceURL();
		this.matchScore = normalised.getMatchScore();
		this.affiliationName = normalised.getAffiliationName();
		this.affiliationNamesID = normalised.getAffiliationNamesID();
		this.affiliation = normalised.getAffiliation();
		
	}

	@Override
	public int getDbID() {
		return this.dbID;
	}

	@Override
	public void setDbID(int dbID) {
		this.dbID = dbID;
	}

	@Override
	public int getParentID() {
		return this.parentID;
	}

	@Override
	public void setParentID(int parentID) {
		this.parentID = parentID;
	}
	
	@Override
	public String serialise() {
		String serialised = "";
		serialised += this.getDbID() + NamesRecord.attributeDelimiter;
		serialised += this.getParentID() + NamesRecord.attributeDelimiter;
		serialised += this.getSourceID() + NamesRecord.attributeDelimiter;
		serialised += this.getSourceName() + NamesRecord.attributeDelimiter;
		serialised += this.getSourceURL() + NamesRecord.attributeDelimiter;
		serialised += this.getMatchScore() + NamesRecord.attributeDelimiter;
		serialised += this.getAffiliationName() + NamesRecord.attributeDelimiter;
		serialised += this.getAffiliationNamesID();
		return serialised;
	}

	@Override
	public void deserialise(String serialised) {
		String[] split = serialised.split(NamesRecord.attributeDelimiter);
		this.setDbID(Integer.parseInt(split[0]));
		this.setParentID(Integer.parseInt(split[1]) );
		this.setSourceID(split[2]);
		this.setSourceName(split[3]);
		this.setSourceURL(split[4]);
		this.setMatchScore(Double.parseDouble(split[5]));
		this.setAffiliationName(split[6]);
		this.setAffiliationNamesID(Integer.parseInt(split[7]));
		
	}
	
	

}
