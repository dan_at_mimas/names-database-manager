package uk.ac.mimas.names.databasemanager.types;

import uk.ac.mimas.names.disambiguator.types.NormalisedTitle;
public class Title extends NormalisedTitle implements Summary {
	private int dbID;
	private int parentID;

	
	/**
	 * 
	 */
	public Title() {
		super();
	}

	/**
	 * @param dbID
	 * @param parentID
	 */
	public Title(int dbID, int parentID) {
		super();
		this.dbID = dbID;
		this.parentID = parentID;
	}
	
	public Title(NormalisedTitle normalised){
		this.dbID = 0;
		this.parentID = 0;
		this.sourceID = normalised.getSourceID();
		this.sourceName = normalised.getSourceName();
		this.sourceURL = normalised.getSourceURL();
		this.matchScore = normalised.getMatchScore();
		this.title  = normalised.getTitle();
	}

	
	/**
	 * @return the dbID
	 */
	public int getDbID() {
		return dbID;
	}

	/**
	 * @param dbID the dbID to set
	 */
	public void setDbID(int dbID) {
		this.dbID = dbID;
	}

	/**
	 * @return the parentID
	 */
	public int getParentID() {
		return parentID;
	}

	/**
	 * @param parentID the parentID to set
	 */
	public void setParentID(int parentID) {
		this.parentID = parentID;
	}

	public String serialise(){
		String serialised = "";
		serialised += this.getDbID() + NamesRecord.attributeDelimiter;
		serialised += this.getParentID() + NamesRecord.attributeDelimiter;
		serialised += this.getSourceID() + NamesRecord.attributeDelimiter;
		serialised += this.getSourceName() + NamesRecord.attributeDelimiter;
		serialised += this.getSourceURL() + NamesRecord.attributeDelimiter;
		serialised += this.getMatchScore() + NamesRecord.attributeDelimiter;
		serialised += this.getTitle();
		return serialised;
	}

	public void deserialise(String serialised){
		String[] split = serialised.split(NamesRecord.attributeDelimiter);
		this.setDbID(Integer.parseInt(split[0]));
		this.setParentID(Integer.parseInt(split[1]) );
		this.setSourceID(split[2]);
		this.setSourceName(split[3]);
		this.setSourceURL(split[4]);
		this.setMatchScore(Double.parseDouble(split[5]));
		this.setTitle(split[6]);

	}
	
	
}
