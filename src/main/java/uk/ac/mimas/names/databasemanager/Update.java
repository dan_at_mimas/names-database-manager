package uk.ac.mimas.names.databasemanager;
import org.apache.log4j.Logger;
import java.sql.*;
import uk.ac.mimas.names.databasemanager.types.*;
import uk.ac.mimas.names.disambiguator.types.*;
import java.util.ArrayList;

public class Update{
	private static final Logger logger = Logger.getLogger(Update.class.getName());

	/**
	 * Takes an arraylist of names records and inserts or updates 
	 * them in the given names database instance
	 *
	 * @param namesRecords the names records to add / update in the database
	 *
	 **/

	public static Response createAndUpdate(ArrayList<NamesRecord> namesRecords, Connection conn){
		 Response response = new Response(Response.NOT_SET, "Update not performed");

		 String createNamesRecordString =
			        "insert into names_records (DATE_CREATED , DATE_MODIFIED, ENTITY_TYPE) VALUES (NOW(),NOW(), ?)";
		 
		 String updateSummariesString = 
				 "update ignore names_records set NAMES_ID = ?, DATE_MODIFIED = NOW(), NAME_ORDER = ? , REDIRECT = ?, NAME_ENTITIES =?, TITLES =?, FIELDS_OF_ACTIVITY =?, RESULT_PUBLICATIONS =?, AFFILIATIONS =?, IDENTIFIERS=? where id = ?";
		 
		 String insertNameString =
				 "insert ignore into name_summaries (RECORD_ID, MATCH_SCORE, SOURCE_ID, SOURCE_NAME, SOURCE_URL, CHARS, USED_FROM, USED_TO) VALUES (?,?,?,?,?,?,?,?)";
		 
		 String insertResultPublicationString =
				 "insert ignore into result_publication_summaries (RECORD_ID, MATCH_SCORE,  SOURCE_ID, SOURCE_NAME, SOURCE_URL, TITLE, NUMBER_OF_RESULT_PUBLICATION, VOLUME_OF_RESULT_PUBLICATION, EDITION, SERIES, ISSUE, DATE_OF_PUBLICATION, ABSTRACT) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)";
		 
		 String insertTitleString =
				 "insert ignore into title_summaries (RECORD_ID, MATCH_SCORE, SOURCE_ID, SOURCE_NAME, SOURCE_URL, TITLE) VALUES (?,?,?,?,?,?)";
		 
		 String insertFieldOfActivityString = 
				 "insert ignore into field_of_activity_summaries (RECORD_ID, MATCH_SCORE, SOURCE_ID, SOURCE_NAME, SOURCE_URL, FIELD_OF_ACTIVITY) VALUES (?,?,?,?,?,?)";
		
		 String insertAffiliationString = 
				 "insert ignore into affiliations_summaries (RECORD_ID,MATCH_SCORE, SOURCE_ID,SOURCE_NAME,SOURCE_URL,INSTITUTION_ID, INSTITUTION_NAME) VALUES (?,?,?,?,?,?,?)";
		 
		 String insertIdentifiersString =
				 "insert ignore into identifiers_summaries (RECORD_ID, MATCH_SCORE, SOURCE_ID,SOURCE_NAME,SOURCE_URL,IDENTIFIER,BASIS_FOR) VALUES(?,?,?,?,?,?,?)";
		 
		 try {
		
		
			
	            conn.setAutoCommit(false);
	           
	            PreparedStatement cstmt, pstmt;
	            ResultSet keys;
	            ArrayList toRemove, toAdd;
	            for(NamesRecord namesRecord : namesRecords){
	            	// if dbid == 0 create a new record
	            	if(namesRecord.getDbID() == 0){
	            		cstmt = conn.prepareStatement(createNamesRecordString, PreparedStatement.RETURN_GENERATED_KEYS);
		            	cstmt.setShort(1, namesRecord.getType());
		            	cstmt.execute();
		            	ResultSet rs = cstmt.getGeneratedKeys();
		            	rs.first();
		            	namesRecord.setDbID(rs.getInt(1));
		            	cstmt.close();
	            	}
	            	
	            	
	            	
	            	

	            	// insert any names
	            	pstmt = conn.prepareStatement(insertNameString, PreparedStatement.RETURN_GENERATED_KEYS);
	            	for(NormalisedName name: namesRecord.getNormalisedNames()){
	            		if(name instanceof Name && ((Name)name).getDbID() > 0)
	            			continue;
	            		pstmt.setInt(1, namesRecord.getDbID());
	            		pstmt.setDouble(2, name.getMatchScore());
	            		pstmt.setString(3, name.getSourceID());
	            		pstmt.setString(4, name.getSourceName());
	            		pstmt.setString(5, name.getSourceURL());
	            		pstmt.setString(6, name.getName());
	            		pstmt.setDate(7, new java.sql.Date(name.getUsedFrom().getTime()));
	            		pstmt.setDate(8, new java.sql.Date(name.getUsedUntil().getTime()));
	            		pstmt.addBatch();
	            	}
	            	pstmt.executeBatch();
	            	
	            	keys = pstmt.getGeneratedKeys();
	            	
	            	toRemove = new ArrayList<NormalisedName>();
	            	toAdd = new ArrayList<Name>();
	            	for(NormalisedName normalisedName : namesRecord.getNormalisedNames()){
	            		if(normalisedName instanceof Name && ((Name)normalisedName).getDbID() > 0)
	            			continue;
	            		if(keys.next() == false)
	                		break;
	            		Name n = new Name(normalisedName);
	            		n.setDbID(keys.getInt(1));
	                	n.setParentID(namesRecord.getDbID());
	                	toAdd.add(n);
	                	toRemove.add(normalisedName);
	            	}
	            	namesRecord.getNormalisedNames().addAll(toAdd);
		            namesRecord.getNormalisedNames().removeAll(toRemove);
		                
	            
	            	
	            	
	            	
	            	// insert any titles
	            	pstmt = conn.prepareStatement(insertTitleString, PreparedStatement.RETURN_GENERATED_KEYS);
	            	for(NormalisedTitle title: namesRecord.getNormalisedTitles()){
	            		
	            		if(title instanceof Title && ((Title) title).getDbID() > 0)
	            			continue; // ignore existing entries for now
	            		
	            		pstmt.setInt(1,namesRecord.getDbID());
	            		pstmt.setDouble(2, title.getMatchScore());
	            		pstmt.setString(3, title.getSourceID());
	            		pstmt.setString(4, title.getSourceName());
	            		pstmt.setString(5, title.getSourceURL());
	            		pstmt.setString(6, title.getTitle());
	            		pstmt.addBatch();
	            		
	            	}
	            	
	            	 pstmt.executeBatch();

	            	 keys = pstmt.getGeneratedKeys();
		            	
	            	toRemove = new ArrayList<NormalisedTitle>();
	            	toAdd = new ArrayList<Title>();
	            	for(NormalisedTitle normalisedTitle : namesRecord.getNormalisedTitles()){
	            		if(normalisedTitle instanceof Title && ((Title)normalisedTitle).getDbID() > 0)
	            			continue;
	            		if(keys.next() == false)
	                		break;
	            		Title n = new Title(normalisedTitle);
	            		n.setDbID(keys.getInt(1));
	                	n.setParentID(namesRecord.getDbID());
	                	toAdd.add(n);
	                	toRemove.add(normalisedTitle);
	            	}
	            	namesRecord.getNormalisedTitles().addAll(toAdd);
		            namesRecord.getNormalisedTitles().removeAll(toRemove);
	            	
	           
	            	
	            	// insert any result publications
	            	pstmt = conn.prepareStatement(insertResultPublicationString, PreparedStatement.RETURN_GENERATED_KEYS);
	               	for(NormalisedResultPublication resultPublication: namesRecord.getNormalisedResultPublications()){
	               		if(resultPublication instanceof ResultPublication && ((ResultPublication) resultPublication).getDbID() >0)
	               			continue;
	            		pstmt.setInt(1, namesRecord.getDbID());
	            		pstmt.setDouble(2, resultPublication.getMatchScore());
	            		pstmt.setString(3, resultPublication.getSourceID());
	            		pstmt.setString(4, resultPublication.getSourceName());
	            		pstmt.setString(5, resultPublication.getSourceURL());
	            		pstmt.setString(6, resultPublication.getTitle());
	            		pstmt.setString(7, resultPublication.getNumber());
	            		pstmt.setString(8, resultPublication.getVolume());
	            		pstmt.setString(9, resultPublication.getEdition());
	            		pstmt.setString(10, resultPublication.getSeries());
	            		pstmt.setString(11, resultPublication.getIssue());
	            		pstmt.setString(12, resultPublication.getDateOfPublication());
	            		pstmt.setString(13, resultPublication.getAbs());
	            		pstmt.addBatch();
	            	}
	            	pstmt.executeBatch();
	            	

	            	keys = pstmt.getGeneratedKeys();
		            	
	            	toRemove = new ArrayList<NormalisedResultPublication>();
	            	toAdd = new ArrayList<ResultPublication>();
	            	for(NormalisedResultPublication normalisedResultPublication : namesRecord.getNormalisedResultPublications()){
	            		if(normalisedResultPublication instanceof ResultPublication && ((ResultPublication)normalisedResultPublication).getDbID() > 0)
	            			continue;
	            		if(keys.next() == false)
	                		break;
	            		ResultPublication n = new ResultPublication(normalisedResultPublication);
	            		n.setDbID(keys.getInt(1));
	                	n.setParentID(namesRecord.getDbID());
	                	toAdd.add(n);
	                	toRemove.add(normalisedResultPublication);
	            	}
	            	namesRecord.getNormalisedResultPublications().addAll(toAdd);
		            namesRecord.getNormalisedResultPublications().removeAll(toRemove);
	            	
	            	
	            	// insert any field of activity
	            	pstmt = conn.prepareStatement(insertFieldOfActivityString, PreparedStatement.RETURN_GENERATED_KEYS);
	            	for(NormalisedFieldOfActivity foi: namesRecord.getNormalisedFieldsOfActivity()){
	            		if(foi instanceof FieldOfActivity && ((FieldOfActivity) foi).getDbID() > 0)
	            			continue;
	            		pstmt.setInt(1, namesRecord.getDbID());
	            		pstmt.setDouble(2, foi.getMatchScore());
	            		pstmt.setString(3, foi.getSourceID());
	            		pstmt.setString(4, foi.getSourceName());
	            		pstmt.setString(5, foi.getSourceURL());
	            		pstmt.setString(6, foi.getFieldOfActivity());
	            		pstmt.addBatch();
	            	}
	            	
	            	pstmt.executeBatch();
	            	

	            	keys = pstmt.getGeneratedKeys();
		            	
	            	toRemove = new ArrayList<NormalisedFieldOfActivity>();
	            	toAdd = new ArrayList<FieldOfActivity>();
	            	for(NormalisedFieldOfActivity normalisedFieldOfActivity : namesRecord.getNormalisedFieldsOfActivity()){
	            		if(normalisedFieldOfActivity instanceof FieldOfActivity && ((FieldOfActivity)normalisedFieldOfActivity).getDbID() > 0)
	            			continue;
	            		if(keys.next() == false)
	                		break;
	            		FieldOfActivity n = new FieldOfActivity(normalisedFieldOfActivity);
	            		n.setDbID(keys.getInt(1));
	                	n.setParentID(namesRecord.getDbID());
	                	toAdd.add(n);
	                	toRemove.add(normalisedFieldOfActivity);
	            	}
	            	namesRecord.getNormalisedFieldsOfActivity().addAll(toAdd);
		            namesRecord.getNormalisedFieldsOfActivity().removeAll(toRemove);
	            	
		            
		         // insert any affiliations
	            	pstmt = conn.prepareStatement(insertAffiliationString, PreparedStatement.RETURN_GENERATED_KEYS);
	            	for(NormalisedAffiliation affiliation: namesRecord.getNormalisedAffiliations()){
	            		if(affiliation instanceof Affiliation && ((Affiliation)affiliation).getDbID() > 0)
	            			continue;
	            		pstmt.setInt(1, namesRecord.getDbID());
	            		pstmt.setDouble(2, affiliation.getMatchScore());
	            		pstmt.setString(3, affiliation.getSourceID());
	            		pstmt.setString(4,  affiliation.getSourceName());
	            		pstmt.setString(5,  affiliation.getSourceURL());
	            		pstmt.setInt(6,  Integer.valueOf(affiliation.getAffiliationNamesID()));
	            		pstmt.setString(7,  affiliation.getAffiliationName());
	            		pstmt.addBatch();
	            	}
	            	pstmt.executeBatch();
	            	
	            	keys = pstmt.getGeneratedKeys();
	            	
	            	toRemove = new ArrayList<NormalisedAffiliation>();
	            	toAdd = new ArrayList<Affiliation>();
	            	for(NormalisedAffiliation normalisedAffiliation: namesRecord.getNormalisedAffiliations()){
	            		if(normalisedAffiliation instanceof Affiliation && ((Affiliation)normalisedAffiliation).getDbID() > 0)
	            			continue;
	            		if(keys.next() == false)
	                		break;
	            		Affiliation n = new Affiliation(normalisedAffiliation);
	            		n.setDbID(keys.getInt(1));
	                	n.setParentID(namesRecord.getDbID());
	                	toAdd.add(n);
	                	toRemove.add(normalisedAffiliation);
	            	}
	            	namesRecord.getNormalisedAffiliations().addAll(toAdd);
		            namesRecord.getNormalisedAffiliations().removeAll(toRemove);
		                
			         // insert any identifiers
	            	pstmt = conn.prepareStatement(insertIdentifiersString, PreparedStatement.RETURN_GENERATED_KEYS);
	            	for(NormalisedIdentifier identifier: namesRecord.getNormalisedIdentifiers()){
	            		if(identifier instanceof Identifier && ((Identifier)identifier).getDbID() > 0)
	            			continue;
	            		pstmt.setInt(1, namesRecord.getDbID());
	            		pstmt.setDouble(2, identifier.getMatchScore());
	            		pstmt.setString(3, identifier.getSourceID());
	            		pstmt.setString(4,  identifier.getSourceName());
	            		pstmt.setString(5,  identifier.getSourceURL());
	            		pstmt.setString(6,  identifier.getIdentifier());
	            		pstmt.setString(7,  identifier.getBasisFor());
	            		pstmt.addBatch();
	            	}
	            	pstmt.executeBatch();
	            	
	            	keys = pstmt.getGeneratedKeys();
	            	
	            	toRemove = new ArrayList<NormalisedIdentifier>();
	            	toAdd = new ArrayList<Identifier>();
	            	for(NormalisedIdentifier normalisedIdentifier: namesRecord.getNormalisedIdentifiers()){
	            		if(normalisedIdentifier instanceof Identifier && ((Identifier)normalisedIdentifier).getDbID() > 0)
	            			continue;
	            		if(keys.next() == false)
	                		break;
	            		Identifier n = new Identifier(normalisedIdentifier);
	            		n.setDbID(keys.getInt(1));
	                	n.setParentID(namesRecord.getDbID());
	                	toAdd.add(n);
	                	toRemove.add(normalisedIdentifier);
	            	}
	            	namesRecord.getNormalisedIdentifiers().addAll(toAdd);
		            namesRecord.getNormalisedIdentifiers().removeAll(toRemove);
		                
	            	
	            	// update summaries
	            	pstmt = conn.prepareStatement(updateSummariesString);
	            	pstmt.setInt(1, namesRecord.getNamesID());
	            	pstmt.setString(2, namesRecord.getNormalisedNames().isEmpty() ? null: namesRecord.getNormalisedNames().get(0).getName());
	            	
	            	if(namesRecord.getRedirect() <= 0)
	            		pstmt.setNull(3, java.sql.Types.INTEGER);
	            	else
	            		pstmt.setInt(3, namesRecord.getRedirect());
	       
	            	pstmt.setString(4, namesRecord.getNormalisedNames().isEmpty() ? null: namesRecord.serialiseCollection(namesRecord.getNormalisedNames()));
	            	pstmt.setString(5, namesRecord.getNormalisedTitles().isEmpty() ? null: namesRecord.serialiseCollection(namesRecord.getNormalisedTitles()));
	            	pstmt.setString(6, namesRecord.getNormalisedFieldsOfActivity().isEmpty() ? null: namesRecord.serialiseCollection(namesRecord.getNormalisedFieldsOfActivity()));
	            	pstmt.setString(7, namesRecord.getNormalisedResultPublications().isEmpty() ? null: namesRecord.serialiseCollection(namesRecord.getNormalisedResultPublications()));
	            	pstmt.setString(8, namesRecord.getNormalisedAffiliations().isEmpty() ? null: namesRecord.serialiseCollection(namesRecord.getNormalisedAffiliations()));
	            	pstmt.setString(9, namesRecord.getNormalisedIdentifiers().isEmpty() ? null: namesRecord.serialiseCollection(namesRecord.getNormalisedIdentifiers()));
	            	pstmt.setInt(10, namesRecord.getDbID());
	            	//System.out.println(pstmt);
	            	pstmt.execute();
	            	
	         
	            
	            	conn.commit();
	            	pstmt.close();
		            keys.close();
		         	
		           
	            
	    		}
	            response = new Response(Response.OK, "Records updated");

	        } catch (Exception e) {
	            logger.error(e.toString());
	            response = new Response(Response.QUERY_ERROR, "Error updating database (some records may have been successfully committed) - " + e.toString());
	        } 
		
		return response;
	}

}