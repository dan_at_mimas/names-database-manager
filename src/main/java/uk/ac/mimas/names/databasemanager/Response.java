package uk.ac.mimas.names.databasemanager;

import java.util.ArrayList;

public class Response{
	public static final int NOT_SET = 0;
	public static final int OK = 200;
	public static final int BAD_REQUEST = 400;
	public static final int QUERY_ERROR = 500;
	public int status = 0;
	public String message;
	public Results results;
	
	public Response(){
		status = 0;
		message = "";
		results = new Results();
	}

	public Response(int status, String message){
		this.status = status;
		this.message = message;
		this.results = new Results();
	}

	public Response(int status, String message, Results results){
		this.status  = status;
		this.message = message;
		this.results = results;
	}

	public void setStatus(int status){
		this.status = status;
	}

	public int getStatus(){
		return status;
	}

	public void setMessage(String message){
		this.message = message;
	}

	public String getMessage(){
		return message;
	}

	public void setResults(Results results){
		this.results = results;
	}

	public Results getResults(){
		return results;
	}

}