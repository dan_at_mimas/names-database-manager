package uk.ac.mimas.names.databasemanager.types;

/**
 * Summary interface
 * Every DB attribute type must 
 * include methods for setting the database id of the attribute,
 * the parent (entity) database id,
 * and methods for serialising and deserialising the various attributes
 */
public interface Summary {
	public int getDbID();
	public void setDbID(int dbID);
	public int getParentID();
	public void setParentID(int parentID);
	public String serialise();
	public void deserialise(String serialised);

}
