package uk.ac.mimas.names.databasemanager;

import java.util.ArrayList;
import java.util.Arrays;

import uk.ac.mimas.names.databasemanager.types.*;
import uk.ac.mimas.names.disambiguator.NamesDisambiguator;
import uk.ac.mimas.names.disambiguator.types.*;

import java.sql.*;
import org.apache.log4j.Logger;
import uk.ac.mimas.names.disambiguator.types.NormalisedRecord;
public class DatabaseManager {

	private static final Logger logger = Logger.getLogger(NamesDatabaseManager.class.getName());
	public static final String SOURCE_NAME = "NAMES";
	public static final String SOURCE_URL = "http://names.mimas.ac.uk";
	
	public String driver = "";
	public String database = "";
	public String username = "";
	public String password = "";
	public Connection conn = null;

	
	public DatabaseManager(){
	
	}
	
	public DatabaseManager(String driver, String database, String username, String password){
		this.driver = driver;
		this.database = database;
		this.username = username;
		this.password = password;
		
	}
	
	public String getDriver() {
		return driver;
	}

	public void setDriver(String driver) {
		this.driver = driver;
	}


	public String getDatabase() {
		return database;
	}

	public void setDatabase(String database) {
		this.database = database;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	
	public boolean connect(){
		try {
            Class.forName(driver).newInstance();
            conn = DriverManager.getConnection(database, username, password);
        } catch (Exception e) {
            logger.error("Error opening db connection " + e.toString());
            return false;
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException se) {
                	logger.error("Error closing db connection" + se.toString());
                }
            }
        }
		return true;
	}
	
	public boolean close(){
		try {
			if(conn != null){
				  conn.close();
			}
        } catch (Exception e) {
            logger.error("Error closing db connection " + e.toString());
            return false;
        } finally {
        }
		return true;
	} 

	public Connection getConnection(){
		return conn;
	}

	public boolean isConnected(){
		return conn == null ? false : true;
	}

	// Crud methods
	// Summary entries cannot be changed directly
	// Everything must go through a names record to be altered

	// Retrieve methods

	public Response get(int namesID, short entityType){
		 Response response = new Response(Response.NOT_SET, "Query not performed");
		 PreparedStatement pstmt = null;
		 ResultSet rs = null;
		 try {

		 	String selectByNamesID =   "SELECT names_records.* FROM names_records ";

		 	selectByNamesID += " where names_records.entity_type = ? and names_records.names_id = ? group by names_records.id limit 1;";

            pstmt = conn.prepareStatement(selectByNamesID);
            pstmt.setShort(1, entityType);
            pstmt.setInt(2, namesID);
            logger.debug(pstmt);
            pstmt.execute();
			rs = pstmt.getResultSet();
     		NamesRecord result = new NamesRecord();
     		rs.next();
           	result.setFromResultSet(rs);
           	response.setStatus(Response.OK);
           	response.setMessage("Found record.");
           	response.getResults().setCount(1);
           	response.getResults().getRecords().add(result);
        } catch (Exception e) { 
            logger.error(e.toString());
        	response = new Response(Response.QUERY_ERROR, "Error perform search " + e.toString());  
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException ignore) {
                }
            }
             if (pstmt != null) {
                try {
                    pstmt.close();
                } catch (SQLException ignore) {
                }
            }
        }
		return response;
	}

	public Response find(Query query){
		Response response = new Response(Response.NOT_SET, "Query not performed");
		ArrayList<NamesRecord> records = new ArrayList<NamesRecord>();
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try{
			if(!query.validQuery())
			{
				response = new Response(Response.BAD_REQUEST, "The supplied query was not valid");
				return response;
			}

			
			// build base query

			String selectSQL = "";
			ArrayList<String> parsedNames = new ArrayList<String>();
			String namesLogicOperator = "AND";
			if(query.getNames().length() > 0)
			{
				if(query.parseNames(parsedNames, namesLogicOperator) == false){
					response = new Response(Response.BAD_REQUEST, "The supplied query was not valid");
					return response;
				}

				selectSQL += " JOIN name_summaries on name_summaries.record_id = names_records.id AND (";
            	for(String name : parsedNames){
            		if(!name.equals(parsedNames.get(0)))
            			selectSQL += namesLogicOperator;
            		selectSQL += " name_summaries.chars RLIKE ? ";
            	}
            	selectSQL += ") ";
			}

			ArrayList<String> parsedIdentifiers = new ArrayList<String>();
			String identifiersLogicOperator = "AND";
		 	if(query.getIdentifiers().length() > 0){

		 		if(query.parseIdentifiers(parsedIdentifiers,identifiersLogicOperator) == false){
					response = new Response(Response.BAD_REQUEST, "The supplied query was not valid");
					return response;
				}


            	selectSQL += " JOIN identifiers_summaries on identifiers_summaries.record_id = names_records.id AND (";
            	for(String identifier : parsedIdentifiers){
            		if(!identifier.equals(parsedIdentifiers.get(0)))
            			selectSQL += identifiersLogicOperator;
            		selectSQL += " identifiers_summaries.identifier = ? ";
            	}
            	selectSQL += ") ";
            }

            ArrayList<String> parsedFieldsOfActivity = new ArrayList<String>();
			String fieldsOfActivityLogicOperator = "AND";
		 	if(query.getFieldsOfActivity().length() > 0){

		 		if( query.parseFieldsOfActivity(parsedFieldsOfActivity,fieldsOfActivityLogicOperator) == false){
					response = new Response(Response.BAD_REQUEST, "The supplied query was not valid");
					return response;
				}


            	selectSQL += " JOIN field_of_activity_summaries on field_of_activity_summaries.record_id = names_records.id AND (";
            	for(String fieldOfActivity : parsedFieldsOfActivity){
            		if(!fieldOfActivity.equals(parsedFieldsOfActivity.get(0)))
            			selectSQL += " OR ";
            		selectSQL += " field_of_activity_summaries.field_of_activity RLIKE ? ";
            	}
            	selectSQL += ") ";
            }

            ArrayList<String> parsedAffiliations = new ArrayList<String>();
			String  affiliationsLogicOperator = "AND";
		 	if(query.getAffiliations().length() > 0){

		 		if(query.parseAffiliations(parsedAffiliations,affiliationsLogicOperator) == false){
					response = new Response(Response.BAD_REQUEST, "The supplied query was not valid");
					return response;
				}


            	selectSQL += " JOIN affiliations_summaries on affiliations_summaries.record_id = names_records.id AND (";
            	for(String affiliation : parsedAffiliations){
            		if(!affiliation.equals(parsedAffiliations.get(0)))
            			selectSQL += " OR ";
            		selectSQL += " affiliations_summaries.institution_name RLIKE ? ";
            	}
            	selectSQL += ") ";
            }

	        	
        	if(query.isIgnoreRedirects()){
        		selectSQL += " where names_records.redirect is null ";
        	}
	        
	        
	        
	        	

			int pos = 0;

			// perform count
			String countSQL =   "SELECT count(distinct names_records.names_id) FROM names_records ";
			if(query.isGetCount()){
	        		pstmt = conn.prepareStatement(countSQL + selectSQL);	
	        	    if(parsedNames.size() > 0){
	   	            	for(String name : parsedNames){
	   	            		pstmt.setString(++pos,name);
	   	            	}
	   	            }
	   	            if(parsedIdentifiers.size() > 0){
	   	            	for(String identifier : parsedIdentifiers){
	   	            		pstmt.setString(++pos,identifier);
	   	            	}
	   	            }
	   	            if(parsedFieldsOfActivity.size() > 0){
	   	            	for(String  fieldOfActivity : parsedFieldsOfActivity){
	   	            		pstmt.setString(++pos,fieldOfActivity);
	   	            	}
	   	            }
	   	            if(parsedAffiliations.size() > 0){
	   	            	for(String  affiliation : parsedAffiliations){
	   	            		pstmt.setString(++pos,affiliation);
	   	            	}
	   	            }
	   	            pstmt.execute();
	   	           
	                rs = pstmt.getResultSet();
	              	rs.next();

	   	           	response.getResults().setCount(rs.getInt(1));
	   	           	rs.close();
	        		pstmt.close();
	       	}

			// get query results
	       	pos = 0;
	       	String returnSQL =   "SELECT names_records.* FROM names_records ";
	       	String groupBySQL = " group by names_records.names_id ";
	        String limitSQL = " limit ?,?;";

	        pstmt =  conn.prepareStatement(returnSQL + selectSQL + groupBySQL + limitSQL);

	        if(parsedNames.size() > 0){
            	for(String name : parsedNames){
            		pstmt.setString(++pos, name);
            	}
            }

            if(parsedIdentifiers.size() > 0){
            	for(String identifier : parsedIdentifiers){
            		pstmt.setString(++pos, identifier);
            	}
            }

            if(parsedFieldsOfActivity.size() > 0){
            	for(String fieldOfActivity : parsedFieldsOfActivity){
            		pstmt.setString(++pos, fieldOfActivity);
            	}
            }

            if(parsedAffiliations.size() > 0){
            	for(String affiliation : parsedAffiliations){
            		pstmt.setString(++pos, affiliation);
            	}
            }
        
            pstmt.setInt(++pos,query.getStart());
	        pstmt.setInt(++pos,query.getPageMax());
	   
            pstmt.execute();
           
       		rs = pstmt.getResultSet();
           	
           	while(rs.next()){
           		NamesRecord record = new NamesRecord();
           		record.setFromResultSet(rs);
           		records.add(record);
           	}
           	response.getResults().setRecords(records);
           	response.setStatus(Response.OK);
           	response.setMessage("Query executed.");
           	

           	rs.close();
           	pstmt.close();

		}
		catch(Exception e){
			logger.error(e.toString());
        	response = new Response(Response.QUERY_ERROR, "Error perform search " + e.toString());  
		}
		finally{
			if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException ignore) {
                }
            }
			if (pstmt != null) {
                try {
                    pstmt.close();
                } catch (SQLException ignore) {
                }
            }
		}
		return response;
	}

	public Response find(PreparedStatement stmt, Connection conn){
		Response response = new Response(Response.NOT_SET, "Query not performed");
		return response;
	}

	// update methods

	/**
	 * Takes an arraylist of names records and inserts or updates 
	 * them in the given names database instance
	 *
	 * @param namesRecords the names records to add / update in the database
	 *
	 **/

	public Response addUpdate(ArrayList<NamesRecord> namesRecords){
		 Response response = new Response(Response.NOT_SET, "Update not performed");

		 String createNamesRecordString =
			        "insert into names_records (DATE_CREATED , DATE_MODIFIED, ENTITY_TYPE) VALUES (NOW(),NOW(), ?)";
		 
		 String updateSummariesString = 
				 "update ignore names_records set NAMES_ID = ?, DATE_MODIFIED = NOW(), NAME_ORDER = ? , REDIRECT = ?, NAME_ENTITIES =?, TITLES =?, FIELDS_OF_ACTIVITY =?, RESULT_PUBLICATIONS =?, AFFILIATIONS =?, IDENTIFIERS=? where id = ?";
		 
		 String insertNameString =
				 "insert ignore into name_summaries (RECORD_ID, MATCH_SCORE, SOURCE_ID, SOURCE_NAME, SOURCE_URL, CHARS, USED_FROM, USED_TO) VALUES (?,?,?,?,?,?,?,?)";
		 
		 String insertResultPublicationString =
				 "insert ignore into result_publication_summaries (RECORD_ID, MATCH_SCORE,  SOURCE_ID, SOURCE_NAME, SOURCE_URL, TITLE, NUMBER_OF_RESULT_PUBLICATION, VOLUME_OF_RESULT_PUBLICATION, EDITION, SERIES, ISSUE, DATE_OF_PUBLICATION, ABSTRACT) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)";
		 
		 String insertTitleString =
				 "insert ignore into title_summaries (RECORD_ID, MATCH_SCORE, SOURCE_ID, SOURCE_NAME, SOURCE_URL, TITLE) VALUES (?,?,?,?,?,?)";
		 
		 String insertFieldOfActivityString = 
				 "insert ignore into field_of_activity_summaries (RECORD_ID, MATCH_SCORE, SOURCE_ID, SOURCE_NAME, SOURCE_URL, FIELD_OF_ACTIVITY) VALUES (?,?,?,?,?,?)";
		
		 String insertAffiliationString = 
				 "insert ignore into affiliations_summaries (RECORD_ID,MATCH_SCORE, SOURCE_ID,SOURCE_NAME,SOURCE_URL,INSTITUTION_ID, INSTITUTION_NAME) VALUES (?,?,?,?,?,?,?)";
		 
		 String insertIdentifiersString =
				 "insert ignore into identifiers_summaries (RECORD_ID, MATCH_SCORE, SOURCE_ID,SOURCE_NAME,SOURCE_URL,IDENTIFIER,BASIS_FOR) VALUES(?,?,?,?,?,?,?)";
		 
		 try {
		
		
			
	            conn.setAutoCommit(false);
	           
	            PreparedStatement cstmt, pstmt;
	            ResultSet keys;
	            ArrayList toRemove, toAdd;
	            for(NamesRecord namesRecord : namesRecords){
	            	// if dbid == 0 create a new record
	            	if(namesRecord.getDbID() == 0){
	            		cstmt = conn.prepareStatement(createNamesRecordString, PreparedStatement.RETURN_GENERATED_KEYS);
		            	cstmt.setShort(1, namesRecord.getType());
		            	cstmt.execute();
		            	ResultSet rs = cstmt.getGeneratedKeys();
		            	rs.first();
		            	namesRecord.setDbID(rs.getInt(1));
		            	cstmt.close();
	            	}
	            	
	            	
	            	
	            	

	            	// insert any names
	            	pstmt = conn.prepareStatement(insertNameString, PreparedStatement.RETURN_GENERATED_KEYS);
	            	for(NormalisedName name: namesRecord.getNormalisedNames()){
	            		if(name instanceof Name && ((Name)name).getDbID() > 0)
	            			continue;
	            		pstmt.setInt(1, namesRecord.getDbID());
	            		pstmt.setDouble(2, name.getMatchScore());
	            		pstmt.setString(3, name.getSourceID());
	            		pstmt.setString(4, name.getSourceName());
	            		pstmt.setString(5, name.getSourceURL());
	            		pstmt.setString(6, name.getName());
	            		pstmt.setDate(7, new java.sql.Date(name.getUsedFrom().getTime()));
	            		pstmt.setDate(8, new java.sql.Date(name.getUsedUntil().getTime()));
	            		pstmt.addBatch();
	            	}
	            	pstmt.executeBatch();
	            	
	            	keys = pstmt.getGeneratedKeys();
	            	
	            	toRemove = new ArrayList<NormalisedName>();
	            	toAdd = new ArrayList<Name>();
	            	for(NormalisedName normalisedName : namesRecord.getNormalisedNames()){
	            		if(normalisedName instanceof Name && ((Name)normalisedName).getDbID() > 0)
	            			continue;
	            		if(keys.next() == false)
	                		break;
	            		Name n = new Name(normalisedName);
	            		n.setDbID(keys.getInt(1));
	                	n.setParentID(namesRecord.getDbID());
	                	toAdd.add(n);
	                	toRemove.add(normalisedName);
	            	}
	            	namesRecord.getNormalisedNames().addAll(toAdd);
		            namesRecord.getNormalisedNames().removeAll(toRemove);
		                
	            
	            	
	            	
	            	
	            	// insert any titles
	            	pstmt = conn.prepareStatement(insertTitleString, PreparedStatement.RETURN_GENERATED_KEYS);
	            	for(NormalisedTitle title: namesRecord.getNormalisedTitles()){
	            		
	            		if(title instanceof Title && ((Title) title).getDbID() > 0)
	            			continue; // ignore existing entries for now
	            		
	            		pstmt.setInt(1,namesRecord.getDbID());
	            		pstmt.setDouble(2, title.getMatchScore());
	            		pstmt.setString(3, title.getSourceID());
	            		pstmt.setString(4, title.getSourceName());
	            		pstmt.setString(5, title.getSourceURL());
	            		pstmt.setString(6, title.getTitle());
	            		pstmt.addBatch();
	            		
	            	}
	            	
	            	 pstmt.executeBatch();

	            	 keys = pstmt.getGeneratedKeys();
		            	
	            	toRemove = new ArrayList<NormalisedTitle>();
	            	toAdd = new ArrayList<Title>();
	            	for(NormalisedTitle normalisedTitle : namesRecord.getNormalisedTitles()){
	            		if(normalisedTitle instanceof Title && ((Title)normalisedTitle).getDbID() > 0)
	            			continue;
	            		if(keys.next() == false)
	                		break;
	            		Title n = new Title(normalisedTitle);
	            		n.setDbID(keys.getInt(1));
	                	n.setParentID(namesRecord.getDbID());
	                	toAdd.add(n);
	                	toRemove.add(normalisedTitle);
	            	}
	            	namesRecord.getNormalisedTitles().addAll(toAdd);
		            namesRecord.getNormalisedTitles().removeAll(toRemove);
	            	
	           
	            	
	            	// insert any result publications
	            	pstmt = conn.prepareStatement(insertResultPublicationString, PreparedStatement.RETURN_GENERATED_KEYS);
	               	for(NormalisedResultPublication resultPublication: namesRecord.getNormalisedResultPublications()){
	               		if(resultPublication instanceof ResultPublication && ((ResultPublication) resultPublication).getDbID() >0)
	               			continue;
	            		pstmt.setInt(1, namesRecord.getDbID());
	            		pstmt.setDouble(2, resultPublication.getMatchScore());
	            		pstmt.setString(3, resultPublication.getSourceID());
	            		pstmt.setString(4, resultPublication.getSourceName());
	            		pstmt.setString(5, resultPublication.getSourceURL());
	            		pstmt.setString(6, resultPublication.getTitle());
	            		pstmt.setString(7, resultPublication.getNumber());
	            		pstmt.setString(8, resultPublication.getVolume());
	            		pstmt.setString(9, resultPublication.getEdition());
	            		pstmt.setString(10, resultPublication.getSeries());
	            		pstmt.setString(11, resultPublication.getIssue());
	            		pstmt.setString(12, resultPublication.getDateOfPublication());
	            		pstmt.setString(13, resultPublication.getAbs());
	            		pstmt.addBatch();
	            	}
	            	pstmt.executeBatch();
	            	

	            	keys = pstmt.getGeneratedKeys();
		            	
	            	toRemove = new ArrayList<NormalisedResultPublication>();
	            	toAdd = new ArrayList<ResultPublication>();
	            	for(NormalisedResultPublication normalisedResultPublication : namesRecord.getNormalisedResultPublications()){
	            		if(normalisedResultPublication instanceof ResultPublication && ((ResultPublication)normalisedResultPublication).getDbID() > 0)
	            			continue;
	            		if(keys.next() == false)
	                		break;
	            		ResultPublication n = new ResultPublication(normalisedResultPublication);
	            		n.setDbID(keys.getInt(1));
	                	n.setParentID(namesRecord.getDbID());
	                	toAdd.add(n);
	                	toRemove.add(normalisedResultPublication);
	            	}
	            	namesRecord.getNormalisedResultPublications().addAll(toAdd);
		            namesRecord.getNormalisedResultPublications().removeAll(toRemove);
	            	
	            	
	            	// insert any field of activity
	            	pstmt = conn.prepareStatement(insertFieldOfActivityString, PreparedStatement.RETURN_GENERATED_KEYS);
	            	for(NormalisedFieldOfActivity foi: namesRecord.getNormalisedFieldsOfActivity()){
	            		if(foi instanceof FieldOfActivity && ((FieldOfActivity) foi).getDbID() > 0)
	            			continue;
	            		pstmt.setInt(1, namesRecord.getDbID());
	            		pstmt.setDouble(2, foi.getMatchScore());
	            		pstmt.setString(3, foi.getSourceID());
	            		pstmt.setString(4, foi.getSourceName());
	            		pstmt.setString(5, foi.getSourceURL());
	            		pstmt.setString(6, foi.getFieldOfActivity());
	            		pstmt.addBatch();
	            	}
	            	
	            	pstmt.executeBatch();
	            	

	            	keys = pstmt.getGeneratedKeys();
		            	
	            	toRemove = new ArrayList<NormalisedFieldOfActivity>();
	            	toAdd = new ArrayList<FieldOfActivity>();
	            	for(NormalisedFieldOfActivity normalisedFieldOfActivity : namesRecord.getNormalisedFieldsOfActivity()){
	            		if(normalisedFieldOfActivity instanceof FieldOfActivity && ((FieldOfActivity)normalisedFieldOfActivity).getDbID() > 0)
	            			continue;
	            		if(keys.next() == false)
	                		break;
	            		FieldOfActivity n = new FieldOfActivity(normalisedFieldOfActivity);
	            		n.setDbID(keys.getInt(1));
	                	n.setParentID(namesRecord.getDbID());
	                	toAdd.add(n);
	                	toRemove.add(normalisedFieldOfActivity);
	            	}
	            	namesRecord.getNormalisedFieldsOfActivity().addAll(toAdd);
		            namesRecord.getNormalisedFieldsOfActivity().removeAll(toRemove);
	            	
		            
		         // insert any affiliations
	            	pstmt = conn.prepareStatement(insertAffiliationString, PreparedStatement.RETURN_GENERATED_KEYS);
	            	for(NormalisedAffiliation affiliation: namesRecord.getNormalisedAffiliations()){
	            		if(affiliation instanceof Affiliation && ((Affiliation)affiliation).getDbID() > 0)
	            			continue;
	            		pstmt.setInt(1, namesRecord.getDbID());
	            		pstmt.setDouble(2, affiliation.getMatchScore());
	            		pstmt.setString(3, affiliation.getSourceID());
	            		pstmt.setString(4,  affiliation.getSourceName());
	            		pstmt.setString(5,  affiliation.getSourceURL());
	            		pstmt.setInt(6,  Integer.valueOf(affiliation.getAffiliationNamesID()));
	            		pstmt.setString(7,  affiliation.getAffiliationName());
	            		pstmt.addBatch();
	            	}
	            	pstmt.executeBatch();
	            	
	            	keys = pstmt.getGeneratedKeys();
	            	
	            	toRemove = new ArrayList<NormalisedAffiliation>();
	            	toAdd = new ArrayList<Affiliation>();
	            	for(NormalisedAffiliation normalisedAffiliation: namesRecord.getNormalisedAffiliations()){
	            		if(normalisedAffiliation instanceof Affiliation && ((Affiliation)normalisedAffiliation).getDbID() > 0)
	            			continue;
	            		if(keys.next() == false)
	                		break;
	            		Affiliation n = new Affiliation(normalisedAffiliation);
	            		n.setDbID(keys.getInt(1));
	                	n.setParentID(namesRecord.getDbID());
	                	toAdd.add(n);
	                	toRemove.add(normalisedAffiliation);
	            	}
	            	namesRecord.getNormalisedAffiliations().addAll(toAdd);
		            namesRecord.getNormalisedAffiliations().removeAll(toRemove);
		                
			         // insert any identifiers
	            	pstmt = conn.prepareStatement(insertIdentifiersString, PreparedStatement.RETURN_GENERATED_KEYS);
	            	for(NormalisedIdentifier identifier: namesRecord.getNormalisedIdentifiers()){
	            		if(identifier instanceof Identifier && ((Identifier)identifier).getDbID() > 0)
	            			continue;
	            		pstmt.setInt(1, namesRecord.getDbID());
	            		pstmt.setDouble(2, identifier.getMatchScore());
	            		pstmt.setString(3, identifier.getSourceID());
	            		pstmt.setString(4,  identifier.getSourceName());
	            		pstmt.setString(5,  identifier.getSourceURL());
	            		pstmt.setString(6,  identifier.getIdentifier());
	            		pstmt.setString(7,  identifier.getBasisFor());
	            		pstmt.addBatch();
	            	}
	            	pstmt.executeBatch();
	            	
	            	keys = pstmt.getGeneratedKeys();
	            	
	            	toRemove = new ArrayList<NormalisedIdentifier>();
	            	toAdd = new ArrayList<Identifier>();
	            	for(NormalisedIdentifier normalisedIdentifier: namesRecord.getNormalisedIdentifiers()){
	            		if(normalisedIdentifier instanceof Identifier && ((Identifier)normalisedIdentifier).getDbID() > 0)
	            			continue;
	            		if(keys.next() == false)
	                		break;
	            		Identifier n = new Identifier(normalisedIdentifier);
	            		n.setDbID(keys.getInt(1));
	                	n.setParentID(namesRecord.getDbID());
	                	toAdd.add(n);
	                	toRemove.add(normalisedIdentifier);
	            	}
	            	namesRecord.getNormalisedIdentifiers().addAll(toAdd);
		            namesRecord.getNormalisedIdentifiers().removeAll(toRemove);
		                
	            	
	            	// update summaries
	            	pstmt = conn.prepareStatement(updateSummariesString);
	            	pstmt.setInt(1, namesRecord.getNamesID());
	            	pstmt.setString(2, namesRecord.getNormalisedNames().isEmpty() ? null: namesRecord.getNormalisedNames().get(0).getName());
	            	
	            	if(namesRecord.getRedirect() <= 0)
	            		pstmt.setNull(3, java.sql.Types.INTEGER);
	            	else
	            		pstmt.setInt(3, namesRecord.getRedirect());
	       
	            	pstmt.setString(4, namesRecord.getNormalisedNames().isEmpty() ? null: namesRecord.serialiseCollection(namesRecord.getNormalisedNames()));
	            	pstmt.setString(5, namesRecord.getNormalisedTitles().isEmpty() ? null: namesRecord.serialiseCollection(namesRecord.getNormalisedTitles()));
	            	pstmt.setString(6, namesRecord.getNormalisedFieldsOfActivity().isEmpty() ? null: namesRecord.serialiseCollection(namesRecord.getNormalisedFieldsOfActivity()));
	            	pstmt.setString(7, namesRecord.getNormalisedResultPublications().isEmpty() ? null: namesRecord.serialiseCollection(namesRecord.getNormalisedResultPublications()));
	            	pstmt.setString(8, namesRecord.getNormalisedAffiliations().isEmpty() ? null: namesRecord.serialiseCollection(namesRecord.getNormalisedAffiliations()));
	            	pstmt.setString(9, namesRecord.getNormalisedIdentifiers().isEmpty() ? null: namesRecord.serialiseCollection(namesRecord.getNormalisedIdentifiers()));
	            	pstmt.setInt(10, namesRecord.getDbID());
	            	//System.out.println(pstmt);
	            	pstmt.execute();
	            	
	         
	            
	            	conn.commit();
	            	pstmt.close();
		            keys.close();
		         	
		           
	            
	    		}
	            response = new Response(Response.OK, "Records updated");

	        } catch (Exception e) {
	            logger.error(e.toString());
	            response = new Response(Response.QUERY_ERROR, "Error updating database (some records may have been successfully committed) - " + e.toString());
	        } 
		
		return response;
	}

}
