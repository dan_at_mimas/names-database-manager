package uk.ac.mimas.names.databasemanager;
import org.apache.log4j.Logger;
import java.sql.*;
import uk.ac.mimas.names.databasemanager.types.*;
import java.util.ArrayList;
public class Find{
private static final Logger logger = Logger.getLogger(Find.class.getName());

	public static Response get(int namesID, short entityType, Connection conn){
		 Response response = new Response(Response.NOT_SET, "Query not performed");
		 PreparedStatement pstmt = null;
		 ResultSet rs = null;
		 try {

		 	String selectByNamesID =   "SELECT names_records.* FROM names_records ";

		 	selectByNamesID += " where names_records.entity_type = ? and names_records.names_id = ? group by names_records.id limit 1;";

            pstmt = conn.prepareStatement(selectByNamesID);
            pstmt.setShort(1, entityType);
            pstmt.setInt(2, namesID);
            logger.debug(pstmt);
            pstmt.execute();
			rs = pstmt.getResultSet();
     		NamesRecord result = new NamesRecord();
     		rs.next();
           	result.setFromResultSet(rs);
           	response.setStatus(Response.OK);
           	response.setMessage("Found record.");
           	response.getResults().setCount(1);
           	response.getResults().getRecords().add(result);
        } catch (Exception e) { 
            logger.error(e.toString());
        	response = new Response(Response.QUERY_ERROR, "Error perform search " + e.toString());  
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException ignore) {
                }
            }
             if (pstmt != null) {
                try {
                    pstmt.close();
                } catch (SQLException ignore) {
                }
            }
        }
		return response;
	}

	public static Response find(Query query, Connection conn){
		Response response = new Response(Response.NOT_SET, "Query not performed");
		ArrayList<NamesRecord> records = new ArrayList<NamesRecord>();
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try{
			if(!query.validQuery())
			{
				response = new Response(Response.BAD_REQUEST, "The supplied query was not valid");
				return response;
			}

			
			// build base query

			String selectSQL = "";
			ArrayList<String> parsedNames = new ArrayList<String>();
			String namesLogicOperator = "AND";
			if(query.getNames().length() > 0)
			{
				if(query.parseNames(parsedNames, namesLogicOperator) == false){
					response = new Response(Response.BAD_REQUEST, "The supplied query was not valid");
					return response;
				}

				selectSQL += " JOIN name_summaries on name_summaries.record_id = names_records.id AND (";
            	for(String name : parsedNames){
            		if(!name.equals(parsedNames.get(0)))
            			selectSQL += namesLogicOperator;
            		selectSQL += " name_summaries.chars RLIKE ? ";
            	}
            	selectSQL += ") ";
			}

			ArrayList<String> parsedIdentifiers = new ArrayList<String>();
			String identifiersLogicOperator = "AND";
		 	if(query.getIdentifiers().length() > 0){

		 		if(query.parseIdentifiers(parsedIdentifiers,identifiersLogicOperator) == false){
					response = new Response(Response.BAD_REQUEST, "The supplied query was not valid");
					return response;
				}


            	selectSQL += " JOIN identifiers_summaries on identifiers_summaries.record_id = names_records.id AND (";
            	for(String identifier : parsedIdentifiers){
            		if(!identifier.equals(parsedIdentifiers.get(0)))
            			selectSQL += identifiersLogicOperator;
            		selectSQL += " identifiers_summaries.identifier = ? ";
            	}
            	selectSQL += ") ";
            }

            ArrayList<String> parsedFieldsOfActivity = new ArrayList<String>();
			String fieldsOfActivityLogicOperator = "AND";
		 	if(query.getFieldsOfActivity().length() > 0){

		 		if( query.parseFieldsOfActivity(parsedFieldsOfActivity,fieldsOfActivityLogicOperator) == false){
					response = new Response(Response.BAD_REQUEST, "The supplied query was not valid");
					return response;
				}


            	selectSQL += " JOIN field_of_activity_summaries on field_of_activity_summaries.record_id = names_records.id AND (";
            	for(String fieldOfActivity : parsedFieldsOfActivity){
            		if(!fieldOfActivity.equals(parsedFieldsOfActivity.get(0)))
            			selectSQL += " OR ";
            		selectSQL += " field_of_activity_summaries.field_of_activity RLIKE ? ";
            	}
            	selectSQL += ") ";
            }

            ArrayList<String> parsedAffiliations = new ArrayList<String>();
			String  affiliationsLogicOperator = "AND";
		 	if(query.getAffiliations().length() > 0){

		 		if(query.parseAffiliations(parsedAffiliations,affiliationsLogicOperator) == false){
					response = new Response(Response.BAD_REQUEST, "The supplied query was not valid");
					return response;
				}


            	selectSQL += " JOIN affiliations_summaries on affiliations_summaries.record_id = names_records.id AND (";
            	for(String affiliation : parsedAffiliations){
            		if(!affiliation.equals(parsedAffiliations.get(0)))
            			selectSQL += " OR ";
            		selectSQL += " affiliations_summaries.institution_name RLIKE ? ";
            	}
            	selectSQL += ") ";
            }

	        	
        	if(query.isIgnoreRedirects()){
        		selectSQL += " where names_records.redirect is null ";
        	}
	        
	        
	        
	        	

			int pos = 0;

			// perform count
			String countSQL =   "SELECT count(distinct names_records.names_id) FROM names_records ";
			if(query.isGetCount()){
	        		pstmt = conn.prepareStatement(countSQL + selectSQL);	
	        	    if(parsedNames.size() > 0){
	   	            	for(String name : parsedNames){
	   	            		pstmt.setString(++pos,name);
	   	            	}
	   	            }
	   	            if(parsedIdentifiers.size() > 0){
	   	            	for(String identifier : parsedIdentifiers){
	   	            		pstmt.setString(++pos,identifier);
	   	            	}
	   	            }
	   	            if(parsedFieldsOfActivity.size() > 0){
	   	            	for(String  fieldOfActivity : parsedFieldsOfActivity){
	   	            		pstmt.setString(++pos,fieldOfActivity);
	   	            	}
	   	            }
	   	            if(parsedAffiliations.size() > 0){
	   	            	for(String  affiliation : parsedAffiliations){
	   	            		pstmt.setString(++pos,affiliation);
	   	            	}
	   	            }
	   	            pstmt.execute();
	   	           
	                rs = pstmt.getResultSet();
	              	rs.next();

	   	           	response.getResults().setCount(rs.getInt(1));
	   	           	rs.close();
	        		pstmt.close();
	       	}

			// get query results
	       	pos = 0;
	       	String returnSQL =   "SELECT names_records.* FROM names_records ";
	       	String groupBySQL = " group by names_records.names_id ";
	        String limitSQL = " limit ?,?;";

	        pstmt =  conn.prepareStatement(returnSQL + selectSQL + groupBySQL + limitSQL);

	        if(parsedNames.size() > 0){
            	for(String name : parsedNames){
            		pstmt.setString(++pos, name);
            	}
            }

            if(parsedIdentifiers.size() > 0){
            	for(String identifier : parsedIdentifiers){
            		pstmt.setString(++pos, identifier);
            	}
            }

            if(parsedFieldsOfActivity.size() > 0){
            	for(String fieldOfActivity : parsedFieldsOfActivity){
            		pstmt.setString(++pos, fieldOfActivity);
            	}
            }

            if(parsedAffiliations.size() > 0){
            	for(String affiliation : parsedAffiliations){
            		pstmt.setString(++pos, affiliation);
            	}
            }
        
            pstmt.setInt(++pos,query.getStart());
	        pstmt.setInt(++pos,query.getPageMax());
	   
            pstmt.execute();
           
       		rs = pstmt.getResultSet();
           	
           	while(rs.next()){
           		NamesRecord record = new NamesRecord();
           		record.setFromResultSet(rs);
           		records.add(record);
           	}
           	response.getResults().setRecords(records);
           	response.setStatus(Response.OK);
           	response.setMessage("Query executed.");
           	

           	rs.close();
           	pstmt.close();

		}
		catch(Exception e){
			logger.error(e.toString());
        	response = new Response(Response.QUERY_ERROR, "Error perform search " + e.toString());  
		}
		finally{
			if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException ignore) {
                }
            }
			if (pstmt != null) {
                try {
                    pstmt.close();
                } catch (SQLException ignore) {
                }
            }
		}
		return response;
	}

	public static Response find(PreparedStatement stmt, Connection conn){
		Response response = new Response(Response.NOT_SET, "Query not performed");
		return response;
	}

}