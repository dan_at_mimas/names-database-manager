package uk.ac.mimas.names.databasemanager;

import java.util.ArrayList;
import org.apache.log4j.Logger;
import java.util.Arrays;
public class Query {
private static final Logger logger = Logger.getLogger(Query.class.getName());
	private int start = 0;
	private int pageMax = 10;
	private boolean getCount = false;
	private boolean ignoreRedirects = true;
	private String names;
	private String affiliations;
	private String familyNames;
	private String givenNames;
	private String fieldsOfActivity;
	private String identifiers;

	
	
	/**
	 * 
	 */
	public Query() {
		super();

		this.names ="";
		this.affiliations ="";
		this.familyNames = "";
		this.givenNames = "";
		this.fieldsOfActivity = "";
		this.identifiers ="";
	}


	/**
	 * @param names
	 * @param affiliations
	 * @param familyNames
	 * @param givenNames
	 * @param fieldsOfActivity
	 * @param identifiers
	 * @param start
	 * @param pageMax
	 * @param getTotal
	 */
	public Query(String names,
			String affiliations, String familyNames,
			String givenNames, String fieldsOfActivity, String identifiers, int start, int pageMax, boolean getCount, boolean ignoreRedirects) {
		super();
		
		this.names = names;
		this.affiliations = affiliations;
		this.familyNames = familyNames;
		this.givenNames = givenNames;
		this.fieldsOfActivity = fieldsOfActivity;
		this.identifiers = identifiers;
		this.start = start;
		this.pageMax = pageMax;
		this.getCount = getCount;
		this.ignoreRedirects = ignoreRedirects;
	}


	

	/**
	 * @return the names
	 */
	public String getNames() {
		return names;
	}


	/**
	 * @param names the names to set
	 */
	public void setNames(String names) {
		this.names = names;
	}


	/**
	 * @return the affiliations
	 */
	public String getAffiliations() {
		return affiliations;
	}


	/**
	 * @param affiliations the affiliations to set
	 */
	public void setAffiliations(String affiliations) {
		this.affiliations = affiliations;
	}


	/**
	 * @return the familyNames
	 */
	public String getFamilyNames() {
		return familyNames;
	}


	/**
	 * @param familyNames the familyNames to set
	 */
	public void setFamilyNames(String familyNames) {
		this.familyNames = familyNames;
	}


	/**
	 * @return the givenNames
	 */
	public String getGivenNames() {
		return givenNames;
	}


	/**
	 * @param givenNames the givenNames to set
	 */
	public void setGivenNames(String givenNames) {
		this.givenNames = givenNames;
	}


	/**
	 * @return the fieldsOfActivity
	 */
	public String getFieldsOfActivity() {
		return fieldsOfActivity;
	}


	/**
	 * @param fieldsOfActivity the fieldsOfActivity to set
	 */
	public void setFieldsOfActivity(String fieldsOfActivity) {
		this.fieldsOfActivity = fieldsOfActivity;
	}
	
	
	
	
	/**
	 * @return the start
	 */
	public int getStart() {
		return start;
	}


	/**
	 * @param start the start to set
	 */
	public void setStart(int start) {
		this.start = start;
	}


	/**
	 * @return the pageMax
	 */
	public int getPageMax() {
		return pageMax;
	}


	/**
	 * @param pageMax the pageMax to set
	 */
	public void setPageMax(int pageMax) {
		this.pageMax = pageMax;
	}

	

	/**
	 * @return the getCount
	 */
	public boolean isGetCount() {
		return getCount;
	}


	/**
	 * @param getCountthe getCount to set
	 */
	public void setGetCount(boolean getCount) {
		this.getCount = getCount;
	}

	

	/**
	 * @return the ignoreRedirects
	 */
	public boolean isIgnoreRedirects() {
		return ignoreRedirects;
	}


	/**
	 * @param ignoreRedirects the ignoreRedirects to set
	 */
	public void setIgnoreRedirects(boolean ignoreRedirects) {
		this.ignoreRedirects = ignoreRedirects;
	}
	
	/**
	 * @return the identifiers
	 */
	public String getIdentifiers() {
		return identifiers;
	}


	/**
	 * @param identifiers the identifiers to set
	 */
	public void setIdentifiers(String identifiers) {
		
					this.identifiers = identifiers;
	}
	
	public static String formatWildcards(String unformatted){
		return unformatted.replaceAll("\\*", "%");
	}

	/**
	 * A name query can contain:
	 * john smith - AND
	 * john OR smith - OR
	 * "John Smith" - exact
	 * Joh* Smi* - Wildcards
	 **/
	public boolean parseNames(ArrayList<String> names, String logicOperator){
		logicOperator = "AND";
		
		try{
			String[] tempnames = splitOr(getNames()); // determine if the OR operator is present
		      
	    	if(tempnames.length > 1){
	    		logicOperator = " OR ";
	    	}
	    	else if(tempnames.length == 1){
	    		tempnames = implode(tempnames, " ").split("[ ]+(?=([^\"]*\"[^\"]*\")*[^\"]*$)");    // split on spaces, excluding exact phrases
	    	}
	   	
	    	for(int i =0; i < tempnames.length;i++){
	    		 tempnames[i] = removeQuotes(tempnames[i]);				 // remove quotes
	    		 tempnames[i] = parseWildCards(tempnames[i]);
	    	}

	    	names = new ArrayList<String>(Arrays.asList(tempnames));
	    }
	    catch(Exception e){
	    	logger.error("Error parsing name query " + e.toString());
	    	return false;
	    }
	    return true;
	}

	/**
	 * A identifiers query can contain:
	 * #001 #002- AND
	 * #001 OR #002 - OR
	 * "ISNI #001" - exact
	 **/
	public boolean parseIdentifiers(ArrayList<String> identifiers, String logicOperator){
		logicOperator = "AND";
		
		try{
			String[] tempidentifiers = splitOr(getIdentifiers()); // determine if the OR operator is present
		      
	    	if(tempidentifiers.length > 1){
	    		logicOperator = " OR ";
	    	}
	    	else if(tempidentifiers.length == 1){
	    		tempidentifiers = implode(tempidentifiers, " ").split("[ ]+(?=([^\"]*\"[^\"]*\")*[^\"]*$)");    // split on spaces, excluding exact phrases
	    	}
	   	
	    	for(int i =0; i < tempidentifiers.length;i++){
	    		 tempidentifiers[i] = removeQuotes(tempidentifiers[i]);				 // remove quotes
	    		
	    	}

	    	identifiers = new ArrayList<String>(Arrays.asList(tempidentifiers));
	    }
	    catch(Exception e){
	    	logger.error("Error parsing identifier query " + e.toString());
	    	return false;
	    }
	    return true;
	}

	public boolean parseFieldsOfActivity(ArrayList<String> fieldsOfActivity, String logicOperator){
		logicOperator = "AND";
		
		try{
			String[] tempfoa = splitOr(getFieldsOfActivity()); // determine if the OR operator is present
		      
	    	if(tempfoa.length > 1){
	    		logicOperator = " OR ";
	    	}
	    	else if(tempfoa.length == 1){
	    		tempfoa = implode(tempfoa, " ").split("[ ]+(?=([^\"]*\"[^\"]*\")*[^\"]*$)");    // split on spaces, excluding exact phrases
	    	}
	   	
	    	for(int i =0; i < tempfoa.length;i++){
	    		 tempfoa[i] = removeQuotes(tempfoa[i]);				 // remove quotes	
	    	}

			fieldsOfActivity = new ArrayList<String>(Arrays.asList(tempfoa));
	    }
	    catch(Exception e){
	    	logger.error("Error parsing fields of activity query " + e.toString());
	    	return false;
	    }
	    return true;
	}

	public boolean parseAffiliations(ArrayList<String> affiliations, String logicOperator){
		logicOperator = "AND";
		
		try{
			String[] tempaffiliations = splitOr(getAffiliations()); // determine if the OR operator is present
		      
	    	if(tempaffiliations.length > 1){
	    		logicOperator = " OR ";
	    	}
	    	else if(tempaffiliations.length == 1){
	    		tempaffiliations = implode(tempaffiliations, " ").split("[ ]+(?=([^\"]*\"[^\"]*\")*[^\"]*$)");    // split on spaces, excluding exact phrases
	    	}
	   	
	    	for(int i =0; i < tempaffiliations.length;i++){
	    		 tempaffiliations[i] = removeQuotes(tempaffiliations[i]);				 // remove quotes	
	    	}

			affiliations = new ArrayList<String>(Arrays.asList(tempaffiliations));
	    }
	    catch(Exception e){
	    	logger.error("Error parsing affiliations query " + e.toString());
	    	return false;
	    }
	    return true;
	}

	private String[] splitOr(String toSplit){
		return toSplit.split("(?i)\\sOR\\s");
	}

	private String[] splitExactPhrase(String toSplit){
		return toSplit.split("[ ]+(?=([^\"]*\"[^\"]*\")*[^\"]*$)");
	}

	private String removeQuotes(String toRemove){
		return toRemove.replaceAll("\"|'", "").trim();
	}

	private String parseWildCards(String toParse){
		String out = toParse;
		 if(toParse.startsWith("*") && ! toParse.endsWith("*") ){		 // deal with wildcards
			 out = toParse.replaceAll("\\*", ".*") + "$";
		 }
		 else if(!toParse.startsWith("*") &&  toParse.endsWith("*")){
			 out = "^" + toParse.replaceAll("\\*", ".*")   ;
		 }
		 else if(toParse.startsWith("*") &&  toParse.endsWith("*")){
			 out = toParse.replaceAll("\\*", ".*");
		 }
		 else{
			out ="[[:<:]]"+toParse+"[[:>:]]";
		 }
		 return out;
	}

	public boolean validQuery(){
		if( (this.start >= 0
			 && this.pageMax > 0) && ( 
			 this.affiliations != ""
			|| this.familyNames != ""
			|| this.fieldsOfActivity!= ""
			|| this.identifiers != ""
			|| this.givenNames != ""
			|| this.names != "")

		)
			return true;
		return false;
	}
	
	private String implode(String[] bits, String glue){
	   String output = "";
	   if(bits.length > 0){
		   for(String bit : bits){
			  if(!bit.equals(bits[0]))
				  output += glue;
			  output += bit;
		   }
	   }
	   return output;
   }

	
	
	
}
