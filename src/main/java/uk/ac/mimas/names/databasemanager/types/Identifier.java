package uk.ac.mimas.names.databasemanager.types;

import uk.ac.mimas.names.disambiguator.types.NormalisedFieldOfActivity;
import uk.ac.mimas.names.disambiguator.types.NormalisedIdentifier;

public class Identifier extends NormalisedIdentifier implements Summary{
	private int dbID;
	private int parentID;

	
	/**
	 * 
	 */
	public Identifier() {
		super();
	}

	/**
	 * @param dbID
	 * @param parentID
	 */
	public Identifier(int dbID, int parentID) {
		super();
		this.dbID = dbID;
		this.parentID = parentID;
	}
	
	public Identifier(NormalisedIdentifier normalised){
		this.dbID = 0;
		this.parentID = 0;
		this.sourceID = normalised.getSourceID();
		this.sourceName = normalised.getSourceName();
		this.sourceURL = normalised.getSourceURL();
		this.matchScore = normalised.getMatchScore();
		this.identifier = normalised.getIdentifier();
		this.basisFor = normalised.getBasisFor();
	}
	@Override
	public int getDbID() {
		return this.dbID;
	}

	@Override
	public void setDbID(int dbID) {
		this.dbID = dbID;
	}

	@Override
	public int getParentID() {
		return this.parentID;
	}

	@Override
	public void setParentID(int parentID) {
		this.parentID = parentID;
	}
	@Override
	public String serialise() {
		String serialised = "";
		serialised += this.getDbID() + NamesRecord.attributeDelimiter;
		serialised += this.getParentID() + NamesRecord.attributeDelimiter;
		serialised += this.getSourceID() + NamesRecord.attributeDelimiter;
		serialised += this.getSourceName() + NamesRecord.attributeDelimiter;
		serialised += this.getSourceURL() + NamesRecord.attributeDelimiter;
		serialised += this.getMatchScore() + NamesRecord.attributeDelimiter;
		serialised += this.getIdentifier() + NamesRecord.attributeDelimiter;
		serialised += this.getBasisFor();
		return serialised;
	}

	@Override
	public void deserialise(String serialised) {
		String[] split = serialised.split(NamesRecord.attributeDelimiter);
		this.setDbID(Integer.parseInt(split[0]));
		this.setParentID(Integer.parseInt(split[1]) );
		this.setSourceID(split[2]);
		this.setSourceName(split[3]);
		this.setSourceURL(split[4]);
		this.setMatchScore(Double.parseDouble(split[5]));
		this.setIdentifier(split[6]);
		this.setBasisFor(split[7]);
		
	}

}
