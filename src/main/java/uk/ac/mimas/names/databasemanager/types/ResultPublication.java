package uk.ac.mimas.names.databasemanager.types;

import uk.ac.mimas.names.disambiguator.types.NormalisedResultPublication;

public class ResultPublication extends NormalisedResultPublication implements Summary {
	private int dbID;
	private int parentID;

	

	/**
	 * 
	 */
	public ResultPublication() {
		super();
	}

	/**
	 * @param title
	 * @param number
	 * @param volume
	 * @param edition
	 * @param series
	 * @param issue
	 * @param dateOfPublication
	 * @param abs
	 * @param dbID
	 * @param parentID
	 */
	public ResultPublication(String title, String number, String volume,
			String edition, String series, String issue,
			String dateOfPublication, String abs, int dbID, int parentID) {
		super(title, number, volume, edition, series, issue, dateOfPublication,
				abs);
		this.dbID = dbID;
		this.parentID = parentID;
	}
	
	/**
	 
	 */
	public ResultPublication(NormalisedResultPublication normalised) {
	
		this.dbID = 0;
		this.parentID = 0;
		this.sourceID = normalised.getSourceID();
		this.sourceName = normalised.getSourceName();
		this.sourceURL = normalised.getSourceURL();
		this.matchScore = normalised.getMatchScore();
		this.abs = normalised.getAbs();
		this.dateOfPublication = normalised.getDateOfPublication();
		this.edition = normalised.getEdition();
		this.issue = normalised.getIssue();
		this.number = normalised.getNumber();
		this.series = normalised.getSeries();
		this.volume = normalised.getVolume();
		this.title  = normalised.getTitle();
	}


	@Override
	public int getDbID() {
		return this.dbID;
	}

	@Override
	public void setDbID(int dbID) {
		this.dbID = dbID;
	}

	@Override
	public int getParentID() {
		return this.parentID;
	}

	@Override
	public void setParentID(int parentID) {
		this.parentID = parentID;
	}
	@Override
	public String serialise(){
		String serialised = "";
		serialised += this.getDbID() + NamesRecord.attributeDelimiter;
		serialised += this.getParentID() + NamesRecord.attributeDelimiter;
		serialised += this.getSourceID() + NamesRecord.attributeDelimiter;
		serialised += this.getSourceName() + NamesRecord.attributeDelimiter;
		serialised += this.getSourceURL() + NamesRecord.attributeDelimiter;
		serialised += this.getMatchScore() + NamesRecord.attributeDelimiter;
		serialised += this.getTitle() + NamesRecord.attributeDelimiter;
		serialised += this.getNumber() + NamesRecord.attributeDelimiter;
		serialised += this.getVolume() + NamesRecord.attributeDelimiter;
		serialised += this.getEdition() + NamesRecord.attributeDelimiter;
		serialised += this.getSeries() + NamesRecord.attributeDelimiter;
		serialised += this.getIssue() + NamesRecord.attributeDelimiter;
		serialised += this.getDateOfPublication() + NamesRecord.attributeDelimiter;
		serialised += this.getAbs();
		
		return serialised;

	}
	
	@Override
	public void deserialise(String serialised){
		String[] split = serialised.split(NamesRecord.attributeDelimiter, -1);
		this.setDbID(Integer.parseInt(split[0]));
		this.setParentID(Integer.parseInt(split[1]) );
		this.setSourceID(split[2]);
		this.setSourceName(split[3]);
		this.setSourceURL(split[4]);
		this.setMatchScore(Double.parseDouble(split[5]));
		this.setTitle(split[6]);
		this.setNumber(split[7]);
		this.setVolume(split[8]);
		this.setEdition(split[9]);
		this.setSeries(split[10]);
		this.setIssue(split[11]);
		this.setDateOfPublication(split[12]);
		this.setAbs(split[13]);
		

	}

	
}
