package uk.ac.mimas.names.databasemanager.types;



import uk.ac.mimas.names.disambiguator.types.*;
import java.util.Date;
import java.util.ArrayList;
import java.util.Map;
import java.sql.ResultSet;


public class NamesRecord extends NormalisedRecord{
   public static String elementDelimiter = "<e>";
   public static String attributeDelimiter = "<f>";
   private int dbID;
   private int namesID;
   private Date dateCreated;
   private Date dateModified;
   private int redirect;
   private int mergeWith;
   private int destroy;
   private String nameOrder;
   private String potentialMatches;
   private String history;
	 
	/**
	*
	* Constructor
	**/
 	public NamesRecord(){
		super();
		this.dbID = 0;
		this.namesID = 0;
		this.dateCreated = new Date(Long.MIN_VALUE);
		this.dateModified =  new Date(Long.MIN_VALUE);
		this.redirect = 0;
		this.mergeWith = 0;
		this.destroy = 0;
		this.nameOrder = "";
		this.potentialMatches = "";
		this.history = "";
	
	}
	/**
	*
	* Constructor
	* @param entityType the base entity type of this record
	**/
	public NamesRecord(short entityType){
		super(entityType);
		this.dbID = 0;
		this.namesID = 0;
		this.dateCreated = new Date(Long.MIN_VALUE);
		this.dateModified =  new Date(Long.MIN_VALUE);
		this.redirect = 0;
		this.mergeWith = 0;
		this.destroy = 0;
		this.nameOrder = "";
		this.potentialMatches = "";
		this.history = "";
		
	}

	/**
	*
	* Constructor
	*
	* @param n a normalised record to convert into a full names db record
	**/
	public NamesRecord(NormalisedRecord n){
		super();
		this.dbID = 0;
		this.namesID = 0;
		this.dateCreated = new Date(Long.MIN_VALUE);
		this.dateModified =  new Date(Long.MIN_VALUE);
		this.redirect = 0;
		this.mergeWith = 0;
		this.destroy = 0;
		this.nameOrder = "";
		this.potentialMatches = "";
		this.history = "";
		
		this.setType(n.getType());
		for(NormalisedTitle normalised: n.getNormalisedTitles()){
			this.getNormalisedTitles().add(new Title(normalised));
		}
		this.getNormalisedAffiliations().addAll(n.getNormalisedAffiliations());
		this.getNormalisedCollaborations().addAll(n.getNormalisedCollaborations());
		this.getNormalisedEarlierNames().addAll(n.getNormalisedEarlierNames());
		this.getNormalisedFieldsOfActivity().addAll(n.getNormalisedFieldsOfActivity());
		this.getNormalisedHomepages().addAll(n.getNormalisedHomepages());
		this.getNormalisedIdentifiers().addAll(n.getNormalisedIdentifiers());
		this.getNormalisedNames().addAll(n.getNormalisedNames());
		this.getNormalisedResultPublications().addAll(n.getNormalisedResultPublications());
		
	}
	/**
	 * @return the dbID
	 */
	public int getDbID() {
		return dbID;
	}

	/**
	 * @param dbID the dbID to set
	 */
	public void setDbID(int dbID) {
		this.dbID = dbID;
	}

	/**
	 * @return the namesID
	 */
	public int getNamesID() {
		return namesID;
	}

	/**
	 * @param namesID the namesID to set
	 */
	public void setNamesID(int namesID) {
		this.namesID = namesID;
	}

	/**
	 * @return the dateCreated
	 */
	public Date getDateCreated() {
		return dateCreated;
	}

	/**
	 * @param dateCreated the dateCreated to set
	 */
	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	/**
	 * @return the dateModified
	 */
	public Date getDateModified() {
		return dateModified;
	}

	/**
	 * @param dateModified the dateModified to set
	 */
	public void setDateModified(Date dateModified) {
		this.dateModified = dateModified;
	}

	/**
	 * @return the redirect
	 */
	public int getRedirect() {
		return redirect;
	}

	/**
	 * @param redirect the redirect to set
	 */
	public void setRedirect(int redirect) {
		this.redirect = redirect;
	}

	/**
	 * @return the mergeWith
	 */
	public int getMergeWith() {
		return mergeWith;
	}

	/**
	 * @param mergeWith the mergeWith to set
	 */
	public void setMergeWith(int mergeWith) {
		this.mergeWith = mergeWith;
	}

	/**
	 * @return the destroy
	 */
	public int getDestroy() {
		return destroy;
	}

	/**
	 * @param destroy the destroy to set
	 */
	public void setDestroy(int destroy) {
		this.destroy = destroy;
	}

	/**
	 * @return the nameOrder
	 */
	public String getNameOrder() {
		return nameOrder;
	}

	/**
	 * @param nameOrder the nameOrder to set
	 */
	public void setNameOrder(String nameOrder) {
		this.nameOrder = nameOrder;
	}

	/**
	 * @return the potentialMatches
	 */
	public String getPotentialMatches() {
		return potentialMatches;
	}

	/**
	 * @param potentialMatches the potentialMatches to set
	 */
	public void setPotentialMatches(String potentialMatches) {
		this.potentialMatches = potentialMatches;
	}

	/**
	 * @return the history
	 */
	public String getHistory() {
		return history;
	}

	/**
	 * @param history the history to set
	 */
	public void setHistory(String history) {
		this.history = history;
	}

			public void setFromResultSet(ResultSet rs){
			try{
				this.setDbID(rs.getInt("ID"));
				this.setType(rs.getShort("ENTITY_TYPE"));
				this.setNamesID(rs.getInt("NAMES_ID"));
				this.setDateCreated(rs.getDate("DATE_CREATED"));
				this.setDateModified(rs.getDate("DATE_MODIFIED"));
				this.setRedirect(rs.getInt("REDIRECT"));
				this.setMergeWith(rs.getInt("MERGE_WITH"));
				this.setDestroy(rs.getShort("DESTROY"));
				this.setNameOrder(rs.getString("NAME_ORDER"));
				this.setPotentialMatches(rs.getString("POTENTIAL_MATCHES"));
				this.setHistory(rs.getString("HISTORY"));
			
					this.getNormalisedNames().addAll(deserialiseCollection(rs.getString("NAME_ENTITIES"),Name.class));
				
					this.getNormalisedTitles().addAll(deserialiseCollection(rs.getString("TITLES"), Title.class));
	
					this.getNormalisedFieldsOfActivity().addAll(deserialiseCollection(rs.getString("FIELDS_OF_ACTIVITY"), FieldOfActivity.class));
	
					this.getNormalisedIdentifiers().addAll(deserialiseCollection(rs.getString("IDENTIFIERS"), Identifier.class));
				
					this.getNormalisedResultPublications().addAll(deserialiseCollection(rs.getString("RESULT_PUBLICATIONS"),ResultPublication.class));
				this.getNormalisedCollaborations().addAll(deserialiseCollection(rs.getString("COLLABORATIONS"), Collaboration.class));
				
				this.getNormalisedAffiliations().addAll(deserialiseCollection(rs.getString("AFFILIATIONS"),Affiliation.class));
				this.getNormalisedHomepages().addAll(deserialiseCollection(rs.getString("HOMEPAGE"), Homepage.class));
				this.getNormalisedEarlierNames().addAll(deserialiseCollection(rs.getString("EARLIER_NAMES"),Name.class));
			
				
				
				
			}
			catch(Exception e){
				
			}
		}
		
		
		



		
		/**
		 * Collection serialisatin / deserialisation needs testing
		 * @param deserialised
		 * @return
		 */
		public<T> String serialiseCollection(ArrayList<T> deserialised){
			
			String serialised = "";
			for(T r : deserialised){
				if(!(r instanceof Summary))
					continue; 
				if(!r.equals(deserialised.get(0)))
					serialised += NamesRecord.elementDelimiter;
				serialised += ((Summary) r).serialise();
			}
			return serialised;
		}
		
		public<T extends Summary> ArrayList<T> deserialiseCollection(String s, Class<T> c){
			ArrayList<T> deserialisedCollection = new ArrayList<T>();
			try{
				if(s == null)
					return deserialisedCollection;
				String[] split = s.split(elementDelimiter);
				for(String element : split){
					T newElement = c.newInstance();
					newElement.deserialise(element);
					deserialisedCollection.add(newElement);
				}
			
			}
			catch(Exception e){
				System.out.println("Error instanciating" + e.toString());
				e.printStackTrace();
			}
			return deserialisedCollection;
		}
	
}
