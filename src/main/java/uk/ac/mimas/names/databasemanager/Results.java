package uk.ac.mimas.names.databasemanager;

import java.util.ArrayList;
import uk.ac.mimas.names.databasemanager.types.NamesRecord;

public class Results {

	private int count;
	private ArrayList<NamesRecord> records;
	
	/**
	 *
	 */
	public Results() {
		super();
		this.count = 0;
		this.records = null;
	}
	/**
	 * @param count
	 * @param records
	 */
	public Results(int count, ArrayList<NamesRecord> records) {
		super();
		this.count = count;
		this.records = records;
	}
	/**
	 * @return the count
	 */
	public int getCount() {
		return count;
	}
	/**
	 * @param count the count to set
	 */
	public void setCount(int count) {
		this.count = count;
	}
	/**
	 * @return the records
	 */
	public ArrayList<NamesRecord> getRecords() {
		return records;
	}
	/**
	 * @param records the records to set
	 */
	public void setRecords(ArrayList<NamesRecord> records) {
		this.records = records;
	}
	
	public NamesRecord first(){
		if(getRecords().size() > 0)
			return getRecords().get(0);
		else
			return null;
	}
	

}
